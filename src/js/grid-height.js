/*
* filename: grid-height.js
* author: jake rego
* desc: The purpose of this file is to grab the height of the grid__cell element,
*       so that the inner element can be set to that height.  
*/

var windowWidth = $( window ).width();

/* This sets the initial height */
$(document).ready( function() {

  windowWidth = $( window ).width();

  // if grid cells are side by side, change height
  if( windowWidth > 760 ) {
   $('.grid__cell--large').css("min-height", $('#js-grid__cell').css("height") );
  }
  else {
   $('.grid__cell--large').css("min-height", "200px"); 
  }

});

/* This allows the page to adjust "responsively" (albeit hacky) */
$(window).resize( function() {

  windowWidth = $( window ).width();

  // if grid cells are side by side, change height
  if( windowWidth > 760 ) {
   $('.grid__cell--large').css("min-height", $('#js-grid__cell').css("height") );
  }
  else {
   $('.grid__cell--large').css("min-height", "200px"); 
  }

});
