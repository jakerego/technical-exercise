/* One Script to rule them all... */

"use strict";
  
/* Load Plugin Variables */
var gulp              = require('gulp'),
    sass              = require('gulp-sass'),
    browserSync       = require('browser-sync'),
    useref            = require('gulp-useref'),
    uglify            = require('gulp-uglify'),
    gulpIf            = require('gulp-if'),
    minifyCSS         = require('gulp-minify-css'),
    imagemin          = require('gulp-imagemin'),
    cache             = require('gulp-cache'),
    del               = require('del'),
    runSequence       = require('run-sequence'),
    autoprefixer      = require('gulp-autoprefixer'),
    plumber           = require('gulp-plumber');
/*************************/

/* default task set.  compiles and reloads sass */
gulp.task( 'default', function (callback) {
  runSequence( [ 'sass', 'browserSync', 'watch' ],
    callback
  )
});

/* task function to monitor files */
/* gulp.task( 'task-name', ['array', 'of tasks to complete before watch'], func()) */
gulp.task( 'watch', ['browserSync', 'sass'], function(){
  gulp.watch( 'src/scss/**/*.scss', ['sass'] );
  gulp.watch( 'src/*.html', browserSync.reload );
  gulp.watch( 'src/js/**/*.js', browserSync.reload );
});

/* task to build prod site files */
gulp.task( 'build', function(callback){
  var tasks = ['sass', 'images', 'fonts', 'useref'];
  console.log( 'Building production files...' );
  runSequence( 'clean:dist',
                tasks, 
               'prefix', 
                callback );
});

/* task to compile sass files */
gulp.task( 'sass', function() {
  return gulp.src( 'src/scss/styles.scss' )
    .pipe( sass() )
    .pipe( gulp.dest( 'src/css' ) )
    .pipe( browserSync.reload({
      stream: true
    }))
});

/* task to configure browser-sync */
gulp.task( 'browserSync', function(){
  browserSync({
    server: {
      baseDir: 'src/'
    },
  })
});

/* optimizing images */
gulp.task( 'images', function() {
  return gulp.src( 'src/images/*.+(png|jpg|gif|svg|ico)' )
    .pipe( cache( imagemin({
      interlaced: true
    })))
    .pipe( gulp.dest( 'dist/images' ) )
});

gulp.task('prefix', function () {
    return gulp.src('src/css/styles.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('dist/css'));
});

/* builds javascript file and minimizes */
gulp.task( 'useref', function(){
  var assets = useref.assets();
  return gulp.src( 'src/*.html' )
    .pipe( assets )
    .pipe( gulpIf( '*.css', minifyCSS() )) // minify if only .css
    .pipe( gulpIf( '*.js', uglify() ))     //uglify only if .js 
    .pipe( assets.restore() )
    .pipe( useref() )
    .pipe( gulp.dest( 'dist' ) )
});

/* copies fonts into the dist location */
gulp.task( 'fonts', function() {
  return gulp.src( 'src/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'))
});

/* cleans the dist file */
gulp.task( 'clean', function() {
  del( 'dist' );
  return cache.clearAll(callback);
});

/* clean all but images */
gulp.task( 'clean:dist', function(callback) {
  return del( ['dist/**/*', '!dist/images', '!dist/images/**/*'], callback);
});